
# Hun Kaal Zonn

This bot is french so if you don't speak french... well, too bad!

Le bot de Notsena.

# Utilisation
Ce bot est à utiliser avec Node.JS (≥16) avec le paquet `discord.js` v14.

`auth.json`
```JSON
{
	"token": "le token d'authentification de votre bot",
 	"master": "votre id d'utilisateurice",
	"notsena": "id de Notsena",
	"disabledTools": ["citation", "ip-checker"],
}
```

# Licence
**Hun Kaal Zonn** est fourni sous Licence Publique Rien À Branler (WTFPL). Pour plus de détails, lisez COPYING.txt, ou ce lien : [http://sam.zoy.org/lprab/COPYING](http://www.wtfpl.net/txt/copying)

![WTFPL](http://www.wtfpl.net/wp-content/uploads/2012/12/logo-220x1601.png)
