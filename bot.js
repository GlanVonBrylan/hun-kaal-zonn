
import "./utils.js";

import { Client, GatewayIntentBits as INTENTS } from "discord.js";
import { readFileSync, readdirSync } from "node:fs";
export const auth = JSON.parse(readFileSync("auth.json"));

export const client = new Client({
	intents: [
		INTENTS.Guilds,
		INTENTS.GuildMessages,
		INTENTS.MessageContent,
		INTENTS.GuildVoiceStates,
	],
});
import loadCommands from "@brylan/djs-commands";

export var myself;
export var master;
export var notsena;

export function sendToMaster(msg, onError = error) { return master.send(msg).catch(onError); }


import "./error.js";
import { handleInteraction as componentInteraction } from "./components.js";

client.login(auth.token);

client.once("ready", async () => {
	myself = client.user;
	master = await client.users.fetch(auth.master);
	notsena = await client.guilds.fetch(auth.notsena);
	console.log(`Connecté en tant que ${client.user.tag} !`);

	const { disabledTools = [] } = auth;
	const enabled = f => f.endsWith(".js") && !disabledTools.includes(f.substring(0, f.length -3));
	for(const tool of readdirSync("tools").filter(enabled))
		import(`./tools/${tool}`);

	loadCommands(client, {
		debug: auth.DEBUG_MODE,
		//ownerServer: auth.notsena,
		defaultDmPermission: true,
		makeEnumsGlobal: true,
	});
});


client.on("interactionCreate", interaction => {
	if(interaction.isMessageComponent())
		componentInteraction(interaction);
});
