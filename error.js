
import { sendToMaster } from "./bot.js";

global.error = error;
process.on("unhandledRejection", error);
export default error;
export function error(err)
{
	if(err instanceof SyntaxError) // from an import probably
		throw err;
	
	let msg = "Une erreur est survenue ; lisez la console pour plus de détails.";

	if(err && err.message)
	{
		const {message} = err;
		const {httpStatus: status} = err;
		if(message === "read ECONNRESET" || status === 403 || status === 404 || status >= 500
			|| message.includes("nsig extraction failed"))
			return;

		msg += err.name === "DiscordAPIError" ? `\nMessage : ${message}\nChemin : ${err.path}` : `\nMessage : ${message}`;
	}

	sendToMaster(msg, console.error);
	console.error(err);
}
