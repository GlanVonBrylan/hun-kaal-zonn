
import { readFile, writeFile } from "node:fs/promises";
import { client } from "../bot.js";
const { users } = client;

const rappels = {};

try {
	Object.assign(rappels, JSON.parse(await readFile("tools/rappels.json")));
	Object.keys(rappels).forEach(start);
} catch {}

var saving = null;
function save()
{
	if(saving)
		saving.then(save);
	else
		saving = writeFile("tools/rappels.json", JSON.stringify(rappels))
			.finally(() => saving = null);
}


export function status()
{
	return Object.values(rappels)
		.map(r => `id: ${r.id}, <@${r.user}> <t:${~~(r.timestamp / 1000)}>`)
		.join("\n")
		|| "*rien*";
}

export function rappelle(user, text, timestamp)
{
	if(user.id)
		user = user.id;
	const id = Date.now();
	rappels[id] = { id, user, text, timestamp };
	start(id);
	save();
}

export function getRappels(user)
{
	const id = user.id || user;
	return Object.values(rappels).filter(({ user }) => user === id);
}

export function annule(id) {
	const rappel = rappels[id];
	if(!rappel) return false;
	clearTimeout(rappel.timeout);
	delete rappels[id];
	save();
	return true;
}


function start(id)
{
	const { user, text, timestamp } = rappels[id];
	const channel = users.fetch(user);
	const timeout = setTimeout(async () => {
		delete rappels[id];
		save();
		(await channel).send("**Rappel**\n" + text);
	}, timestamp - Date.now());
	Object.defineProperty(rappels[id], "timeout", { value: timeout });
}
