
const pingRole = "972661873025646612>";
const idSalon = "377917197445496832";
const idRubrique = "957704834788765737";
var citationAujourdhui = true;
var rubriqueAujourdhui = true;

function dimanche() { return !new Date().getDay(); }

import { auth, client, sendToMaster } from "../bot.js";
const { master } = auth;

{
	const next = new Date();
	if(next.getHours() < 2)
	{
		citationAujourdhui = rubriqueAujourdhui = false;
		next.setHours(20);
		next.setMinutes(0);
		setTimeout(remind, next - Date.now());
	}
	else
		remind();
}


export function status()
{
	return `citationAujourdhui: ${citationAujourdhui}\nrubriqueAujourdhui: ${rubriqueAujourdhui}`;
}


client.on("messageCreate", ({channel, author, content}) => {
	if(author.bot && channel.id === idRubrique && dimanche())
		return rubriqueAujourdhui = true;

	if(channel.id !== idSalon || author.id !== master)
		return;

	if(content.includes(pingRole))
		citationAujourdhui = true;
});


function remind()
{
	if(!citationAujourdhui)
	{
		sendToMaster(`N’oublie pas la <#${idSalon}> !`);
		setTimeout(() => citationAujourdhui || sendToMaster("CITATION J’AI DIT !!"), 3600_000);
	}

	if(!rubriqueAujourdhui && dimanche())
		sendToMaster(`C’est l’heure de la <#${idRubrique}> !`);

	const next = new Date();
	next.setHours(23);
	next.setMinutes(59);

	setTimeout(() => {
		citationAujourdhui = rubriqueAujourdhui = false;
		setTimeout(remind, 72060_000); // 20h 1min
	}, next - Date.now());
}
