
// Checks whenever the IP address changes for whatever reason

const API = "https://api.ipify.org";
var lastHTTPError;

import { sendToMaster } from "../bot.js";

var lastKnownIP, lastCheck;

Number.prototype.date = function() {
	return (this < 10 ? "0" : "") + this;
}

export function status()
{
	return `Dernière IP connue : \`${lastKnownIP}\` (vérifié le ${lastCheck ? `${lastCheck.getDate().date()}/${(lastCheck.getMonth() + 1).date()} à ${lastCheck.getHours().date()}:${lastCheck.getMinutes().date()}` : "*jamais vérifié*"})`;
}


fetch(API).then(async res => {
	lastKnownIP = await res.text();
	lastCheck = new Date();
});

setInterval(async () => {
	const response = await fetch(API).catch(err => err.message.includes("EAI_AGAIN") && console.error(err));
	if(!response) return;
	if(!response.ok)
	{
		const {status, statusText} = response;
		if(lastHTTPError && status === lastHTTPError)
			sendToMaster(`Erreur ${status} ${statusText} en voulant récupérer mon IP.`);
		lastHTTPError = status;
		return;
	}

	lastHTTPError = undefined;
	const currentIP = await response.text().catch(console.error);
	lastCheck = new Date();
	if(!currentIP)
		sendToMaster("Une erreur est survenue en voulant récupérer mon IP ; voir la console pour plus de détails.");
	else if(currentIP.includes("html"))
		sendToMaster(`Erreur en récupérant mon IP :\`\`\`HTML\n${await response.text()}\`\`\``);
	else if(currentIP !== lastKnownIP)
	{
		sendToMaster(`Mon adresse IP vient de changer ! (avant : ${lastKnownIP} ; nouvelle : **${currentIP}**)`);
		lastKnownIP = currentIP;
	}
}, 3600_000);
