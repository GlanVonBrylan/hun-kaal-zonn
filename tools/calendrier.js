
const DATES = {
	"19/01": "fête de l'éloquence",
	"11/02": "anniversaire de Strun",
	"14/03": "fête des flambeurs",
	"27/03": "journée de la presse",
	"11/04": "journée de solidarité aux victimes de la grande catastrophe",
	"22/07": "fête du cube",
	"01/08": "fête des fleurs",
	"12/08": "nocturnale des cons",
	"28/08": "mon anniversaire !",
	"04/11": "fête Notseniale",
	"27/12": "journée des araignées",
};

function datesVariables()
{
	const dates = {};
	const poc = Poc();
	dates[poc._toId()] = "Poc";
	poc.setDate(poc.getDate() + 7);
	dates[poc._toId()] = "dimanche après Poc : fête du son";
	return dates;
}


import { sendToMaster } from "../bot.js";

Date.prototype._toId = function() {
	return `${this.getDate().padStart(2)}/${(this.getMonth()+1).padStart(2)}`;
}

function prepareCheck()
{
	checkDate();
	const tomorrow = new Date();
	tomorrow.setHours(24);
	tomorrow.setMinutes(1);
	setTimeout(prepareCheck, tomorrow - Date.now());
}

prepareCheck();


export function status()
{
	const now = new Date()._toId();
	const events = { ...DATES, ...datesVariables() };
	const dates = Object.keys(events);
	let today = "";
	if(dates.includes(now))
		today = `\n**Aujourd'hui : ${events[now]}**`;
	else
		dates.push(now);

	function value(date) { const [day, month] = date.split("/"); return month*100 + day; }
	dates.sort((a,b) => value(a) - value(b));
	const nowIndex = dates.indexOf(now);

	const prev = dates[nowIndex === 0 ? dates.length - 1 : nowIndex-1];
	const next = dates[nowIndex === dates.length - 1 ? 0 : nowIndex+1];
	return `Dernière : ${prev} (${events[prev]})${today}\nSuivante : ${next} (${events[next]})`;
}


function checkDate(avance = 0)
{
	if(!avance)
		checkDate(7);
	let date = new Date();
	date.setDate(date.getDate() + avance);
	date = date._toId();
	const variableEvent = datesVariables()[date];
	if(variableEvent)
		sendToMaster((avance ? "Dans une semaine : " : "") + variableEvent);
	const event = DATES[date];
	return event ? sendToMaster(`${date} : ${event}`) : false;
}


// algorithme de Butcher-Meeus
function Poc()
{
	const date = new Date();
	const année = date.getFullYear();
	const m = année % 19; // cycle de Méton
	const c = ~~(année / 100); // centaine et rang de l'année
	const u = année % 100;
	const s = ~~(c / 4); // siècle bissextile
	const t = c % 4;
	const p = ~~((c+8) / 25); // cycle de proemptose
	const q = ~~((c-p+1) / 3); // proemptose
	const e = (19*m + c - s - q + 15) % 30; // épacte
	const b = ~~(u / 4); // année bissextile
	const d = u % 4;
	const l = (2*t + 2*b - e - d + 32) % 7; // lettre dominicale
	const h = ~~((m + 11*e + 22*l) / 451); // correction

	date.setMonth(~~((e + l - 7*h + 114) / 31) - 1);
	date.setDate((e + l - 7*h + 114) % 31 + 1);
	return date;
}
