
import { getRappels, annule } from "../tools/rappel.js";
import { selectMenu, buttons, register } from "../components.js";

export const description = "Je vous rappellerai de faire un truc (par défaut dans 4h)";
export const options = [{
	type: SUBCOMMAND, name: "voir",
	description: "Afficher vos rappels",
}, {
	type: SUBCOMMAND, name: "annuler",
	description: "Annuler un ou plusieurs rappels",
}];
export function run(inter)
{
	if(inter.options.getSubcommand() === "annuler")
		annuler(inter);
	else
		afficher(inter);
}


function n(n) {
	return n.padStart(2, 0);
}
function formatDate(date)
{
	return `${n(date.getDate())}/${n(date.getMonth()+1)}/${date.getFullYear()} à ${n(date.getHours())}:${n(date.getMinutes())}`;
}

function afficher(inter)
{
	const liste = getRappels(inter.user).sort((a,b) => a.timestamp - b.timestamp);
	inter.reply({ flags: "Ephemeral", embeds: [
		liste.length ? {
			title: "Vos rappels",
			fields: liste.map(r => ({ name: `<t:${~~(r.timestamp/1000)}>`, value: r.text.substring(0, 1000) })),
		} : { description: "Vous n'avez aucun rappel de prévu." }
	]});
}


function annuler(inter)
{
	const liste = getRappels(inter.user).sort((a,b) => a.timestamp - b.timestamp);
	if(!liste.length)
		return inter.reply({ flags: "Ephemeral", embeds: [{ description: "Vous n'avez aucun rappel de prévu." }]});

	const customId = "annuleRappel"+Date.now();
	const cancelId = `${customId}_cancel`;
	inter.reply({ flags: "Ephemeral", embeds: [{
		title: "Choisissez les rappels à annuler",
		description: "Attention, cette action est irréversible",
	}], components: [
		selectMenu(customId, liste.map(({id, text, timestamp}) => ({
			value: id.toString(),
			label: formatDate(new Date(timestamp)),
			description: text.length > 100 ? text.substring(0, 99) + "…" : text,
		})), 1, liste.length),
		buttons({style: SECONDARY, label: "Annuler", customId: cancelId}),
	]});

	register(cancelId, inter => inter.update({ content: "*Aucun rappel n'a été annulé.*", components: [], embeds: [] }));
	register(customId, inter => {
		const {values} = inter;
		values.forEach(annule);
		inter.update({ components: [], embeds: [{
			description: values.length === 1 ? "Rappel annulé." : "Rappels annulés.",
		}]});
	});
}
