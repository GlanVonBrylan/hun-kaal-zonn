
import { readdirSync } from "node:fs";
import { splitMessage } from "../utils.js";
import { client } from "../bot.js";

export const description = "Commandes admin";
export const defaultMemberPermissions = "0";

export const options = [{
	type: SUBCOMMAND, name: "shutdown", description: "Éteint le bot",
}, {
	type: SUBCOMMAND, name: "uptime", description: "Temps d'activité",
}, {
	type: SUBCOMMAND, name: "eval", description: "Du code à évaluer",
	options: [{
		type: STRING, name: "code", required: true,
		description: "Le code à exécuter (le bot s'appelle 'client')",
	}],
}, {
	type: SUBCOMMAND, name: "status", description: "Obtenir l'état d'un outil",
	options: [{
		type: STRING, name: "outil", required: true,
		description: "L'outil dont vous voulez récupérer le statut.",
		choices: readdirSync("tools")
			.filter(file => file.endsWith(".js"))
			.map(file => ({name: file.substring(0, file.length-3), value: file})),
	}],
}];

export async function run(inter)
{
	const subCommand = inter.options.getSubcommand();

	if(subCommand === "status")
	{
		const { status } = await import(`../tools/${inter.options.getString("outil")}`);
		inter.reply({
			flags: "Ephemeral",
			content: typeof status === "function" ? status() : "Cet outil n'a pas de fonction `status`",
		});
	}
	else if(subCommand === "shutdown")
	{
		inter.reply({flags: "Ephemeral", content: "À plus."}).catch(Function()).finally(() => {
			client.destroy();
			process.exit();
		});
	}
	else if(subCommand === "eval")
	{
		let res;
		try {
			res = eval(inter.options.getString("code", true));
			if(res === undefined)
				inter.reply({flags: "Ephemeral", content: "✅"});
			else
			{
				const send = inter.channel.send.bind(inter.channel);
				function sendMsg(message) {
					splitMessage(message).forEach(send);
				}

				if(res instanceof Promise)
				{
					inter.reply({flags: "Ephemeral", content: "Le résultat arrive (promis)..."});
					res.then(sendMsg, err => sendMsg(`Promise rejected:\n${err}`));
				}
				else
					inter.reply({flags: "Ephemeral", content: `Résultat : ${res}`});
			}
		}
		catch(e) {
			inter.reply({flags: "Ephemeral", content: e.toString()});
		}
	}
	else if(subCommand === "uptime")
	{
		let seconds = ~~process.uptime();
		let days    = ~~(seconds / 84400);
		seconds -= days * 84400;
	    let hours   = ~~(seconds / 3600);
		seconds -= hours * 3600;
		let minutes = ~~(seconds / 60);
		seconds -= minutes * 60;

	    if(hours   < 10) hours   = "0"+hours;
	    if(minutes < 10) minutes = "0"+minutes;
	    if(seconds < 10) seconds = "0"+seconds;

	    inter.reply(`${days} jour${days > 1 ? "s" : ""}, ${hours}:${minutes}:${seconds}`);
	}
	else
		inter.reply({flags: "Ephemeral", content: `Received unknown subcommand (${subCommand})`});
}
