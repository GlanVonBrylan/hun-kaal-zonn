
import { rappelle, getRappels } from "../tools/rappel.js";
const LIMIT = 336; // 14 jours

export const description = "Je vous rappellerai de faire un truc (par défaut dans 4h)";
export const options = [{
	type: STRING, name: "truc", required: true,
	description: "Le truc à te rappeler",
}, {
	type: STRING, name: "heure",
	description: "Dans combien d'heures le rappeler, ou l'heure exacte, ou “demain”",
	autocomplete: true,
}];
export function run(inter)
{
	function reply(message) {
		inter.reply({ flags: "Ephemeral", content: message });
	}

	const existing = getRappels(inter.user).length;
	if(existing === 25)
		return reply("Vous ne pouvez pas avoir plus de 25 rappels prévus à la fois.");

	const truc = inter.options.getString("truc");
	if(truc.length > 1985)
		return reply("Ce truc est trop long");

	if(inter.options.getString("heure") === "##ERR##")
		return reply("Date demandée invalide (dans le passé ou au-delà de 14 jours peut-être ?)");

	const heure = inter.options.getString("heure") || 4;
	const heures = +heure;
	const entier = Number.isInteger(heures);

	if(entier && heures < LIMIT)
	{
		if(heures < 0)
			return reply("Dans le passé ? lol");

		rappelle(inter.user, truc, Date.now() + heures*3600_000);
		reply(`Ok, je te rappelle ça dans ${heures}h.`);
	}
	else
	{
		const { value, name } = entier
			? { value: heures, name: new Date(heures).toFR() }
			: parseHour(heure);
		if(value > 0)
		{
			rappelle(inter.user, truc, value);
			reply(`Ok, je te rappelle ça ${name}.`);
		}
		else
			reply(`Erreur : ${name}.`);
	}
}

export function autocomplete(inter) {
	try {
		const hour = parseHour(inter.options.getFocused(), true);
		inter.respond([hour]);
	} catch(e) {
		console.error(e);
		inter.respond([{name: ("Err: "+e.message).substring(0, 100), value: "-1"}]);
	}
}


const HOUR_REGEXP = /((^|[ 01])[0-9]|2[0-3])[:h][0-5][0-9]((:|mi?n?)[0-5][0-9])?/; // 05:32:50
const DATE_REGEXP = /([0-2][0-9]|3[01])\/(0[0-9]|1[0-2])(\/20[0-9]{2})?/; // 12/02/2023
const ISO_DATE_REGEXP = /20[0-9]{2}-(0[0-9]|1[0-2])-/; // 2023-02-12

function n(num) {
	return num.padStart(2,0);
}
Date.prototype.hourToFR = function() {
	const seconds = this.getSeconds();
	return `à ${n(this.getHours())}:${n(this.getMinutes())}${seconds ? `:${n(seconds)}` : ""}`;
}
Date.prototype.toFR = function() {
	const curYear = new Date().getFullYear();
	const year = this.getFullYear();
	return `le ${n(this.getDate()+"")}/${n(this.getMonth()+1)}${curYear === year ? "" : `/${this.getFullYear()}`} ${this.hourToFR()}`;
}

function parseHour(arg, stringValue = false)
{
	if(typeof arg !== "string")
		throw new TypeError(`'arg' should be a string, got ${typeof arg}`);
	arg = arg.toLowerCase();
	const result = { name: "g pa compri", value: "##ERR##" };
	const hour = arg.match(HOUR_REGEXP)?.[0].replace(/[hHm]/g, ":").replace(/[in]/g, "") || null;
	const date = (arg.match(DATE_REGEXP) || arg.match(ISO_DATE_REGEXP) || [])[0];
	const curDate = new Date();

	if(!arg)
		result.name = "Vas-y, un truc du genre “demain 15h” ou 12/05 à 08:30";
	else if(arg.startsWith("demain"))
	{
		const [h, mn = 0] = (hour || (arg.match(/([ 01][0-9]|2[0-3])(?=h)/)?.[0] || "05")+":00").split(":");
		const targetDate = curDate;
		targetDate.setDate(targetDate.getDate() + 1);
		targetDate.setHours(h); targetDate.setMinutes(mn); targetDate.setSeconds(0);
		result.value = targetDate.getTime();
		result.name = `demain à ${+mn ? `${h}:${mn}` : `${h}h`}`;
	}
	else if(date || hour)
	{
		let targetDate;
		if(date)
		{
			let dateStr;
			if(date.includes("/"))
			{
				const curMonth = curDate.getMonth(), curYear = curDate.getFullYear();
				const [day, month, year] = date.split("/");
				dateStr = `${year ?? (month < curMonth ? curYear + 1 : curYear)}-${month}-${day}`;
			}
			else
				dateStr = date;

			if(hour)
				dateStr += ` ${hour}`;

			targetDate = new Date(dateStr);
			if(targetDate < curDate)
				result.name = "Cette date est dans le passé.";
			else if(targetDate - curDate > LIMIT * 3600_000)
				result.name = "Je ne peux pas faire de rappel au-delà de 14 jours (la faute à JavaScript).";
			else
			{
				result.value = targetDate.getTime();
				result.name = targetDate.toFR();
			}
		}
		else
		{
			const [hours, minutes, seconds = 0] = hour.split(":");
			targetDate = new Date();
			targetDate.setHours(hours); targetDate.setMinutes(minutes); targetDate.setSeconds(seconds);
			result.name = targetDate.hourToFR();
			if(targetDate < curDate)
			{
				targetDate.setDate(targetDate.getDate() + 1);
				result.name = `demain ${result.name}`;
			}
			result.value = targetDate.getTime();
		}
	}
	else if(Number.isInteger(+arg))
	{
		arg = +arg;
		if(arg > LIMIT)
			result.name = "Je ne peux pas faire de rappel au-delà de 14 jours (la faute à JavaScript).";
		else if(arg < 0)
			result.name = "Dans le passé ? T'es rigolo, toi.";
		else
		{
			result.value = Date.now() + arg*3600_000;
			const targetDate = new Date(result.value);
			targetDate.setSeconds(0);
			result.name = arg ? `dans ${arg}h (${targetDate.toFR()})` : "là, tout de suite";
		}
	}

	if(stringValue)
		result.value = ""+result.value;

	return result;
}
