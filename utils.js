
Array.prototype.random = function() {
	return this[~~(Math.random() * this.length)];
}

Array.prototype.shuffle = function() {
    for(let i = this.length - 1 ; i > 0 ; i--)
	{
        const j = ~~(Math.random() * (i + 1));
        [this[i], this[j]] = [this[j], this[i]];
    }
    return this;
}


Number.prototype.padStart = function(targetLength, padString = "0") {
	return this.toString().padStart(targetLength, padString);
}


String.prototype.toSnakeCase = function() {
	// Weird regexes to not accidentally turn strings like HTTPQuery into H_T_T_P_Query
	return this.replace(/(?!^)[A-Z](?=[^A-Z])/g, letter => `_${letter}`)
			.replace(/(?<=[^A-Z])[A-Z](?=[A-Z])/g, letter => `_${letter}`)
			.toUpperCase();
}


export function splitMessage
	(text, { maxLength = 2_000, char = "\n", prepend = "", append = "" } = {})
{
	if(typeof text !== 'string')
		throw new TypeError(`Expected a string, got a ${typeof text}.`);

	if(typeof char !== "string")
		throw new TypeError(`'char' is only allowed to be a string, got a ${char instanceof RegExp ? "RegExp" : typeof char}.`);

	if(text.length <= maxLength)
		return [text];

	const splitText = text.split(char);

	if(splitText.some(elem => elem.length > maxLength))
		throw new RangeError(`A chunk was larger than the maximum length`);

	const messages = [];
	const charL = char.length, prependL = prepend.length, appendL = append.length;
	let msg = "";
	for(const chunk of splitText)
	{
		if(msg && (msg.length + charL + chunk.length + appendL) > maxLength)
		{
			messages.push(msg + append);
			msg = prepend;
		}
		msg += (msg && msg !== prepend ? char : "") + chunk;
	}
	return messages.concat(msg).filter(m => m);
}
